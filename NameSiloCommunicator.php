<?php
/**
 * @file
 * Class source for communicating with NameSilo.com
 */

namespace NameSilo;

/**
 * NameSilo.com communication handler.
 *
 * Serves as a starting point for NameSilo.com API handlers that need to
 * communicate directly with NameSilo.com.
 *
 * @author Steven Wichers
 * @version 1
 * @class
 */
abstract class NameSiloCommunicator {

  /**
   * The NameSilo.com API URL prefix.
   *
   * This URL should not include the version number or any commands.
   */
  const API_URL = 'https://www.namesilo.com/api/';

  /**
   * The NameSilo.com API version to use.
   */
  const API_VERSION = 1;

  /**
   * The NameSilo.com API result code to be considered as a successful request.
   *
   * @see NameSiloCommunicator::resultCodeLookup() to see all available codes.
   */
  const SUCCESS_CODE = 300;

  /**
   * The NameSilo.com API key required for all requests to NameSilo.com
   *
   * Stored for use when making requests to NameSilo.com
   * @var string
   */
  protected $_api_key = NULL;

  /**
   * Construct a NameSiloCommunicator object.
   *
   * @param string $api_key The NameSilo.com API key to use.
   */
  public function __construct($api_key) {

    /**
     * API keys are hex values and should be verified as such.
     */
    if (!ctype_xdigit($api_key)) {

      throw new Exception('Invalid NameSilo API key.');
    }

    $this->_api_key = $api_key;
  }

  /**
   * Perform an API request.
   *
   * @param string $action The API action to perform.
   * @param array $params Necessary parameters for the given API action.
   *
   * @return mixed Returns a SimpleXML object or FALSE on failure.
   *
   * @see https://www.namesilo.com/api_reference.php to view available actions.
   */
  protected function fetch($action, array $params = array()) {

    $defaults = array(
      'version' => self::API_VERSION,
      'type' => 'xml',
      'key' => $this->_api_key,
    );

    $params = $defaults + $params;

    $url = sprintf('%s%s?%s', self::API_URL, $action, http_build_query($params));

    $result = file_get_contents($url);

    if (!empty($result)) {

      $xml = simplexml_load_string($result);
      if (self::SUCCESS_CODE != $xml->reply->code) {

        throw new Exception($this->resultCodeLookup($xml->reply->code));
      }

      return $xml->reply;
    }

    return FALSE;
  }

  /**
   * Translates a NameSilo.com response code to an error message.
   *
   * @param int $code The code to perform a lookup on.
   *
   * @return string The error message corresponding to the given code.
   *
   * @see https://www.namesilo.com/api_reference.php for a list of available
   *      codes and messages.
   */
  protected function resultCodeLookup($code) {

    $codes = array(
      101 => 'HTTPS not used',
      102 => 'No version specified',
      103 => 'Invalid API version',
      104 => 'No type specified',
      105 => 'Invalid API type',
      106 => 'No operation specified',
      107 => 'Invalid API operation',
      108 => 'Missing parameters for the specified operation',
      109 => 'No API key specified',
      110 => 'Invalid API key',
      111 => 'Invalid User',
      112 => 'API not available to Sub-Accounts',
      113 => 'This API account cannot be accessed from your IP',
      114 => 'Invalid Domain Syntax',
      115 => 'Central Registry Not Responding - try again later',
      116 => 'Invalid sandbox account',
      117 => 'The provided credit card profile either does not exist, or is not associated with your account',
      118 => 'The provided credit card profile has not been verified',
      119 => 'Insufficient account funds for requested transaction',
      120 => 'API key must be passed as a GET',
      200 => 'Domain is not active, or does not belong to this user',
      201 => 'Internal system error',
      210 => 'General error (details provided in response)',
      250 => 'Domain is already set to AutoRenew - No update made.',
      251 => 'Domain is already set not to AutoRenew - No update made.',
      252 => 'Domain is already Locked - No update made.',
      253 => 'Domain is already Unlocked - No update made.',
      254 => 'NameServer update cannot be made. (details provided in response)',
      255 => 'Domain is already Private - No update made.',
      256 => 'Domain is already Not Private - No update made.',
      261 => 'Domain processing error (details provided in response)',
      262 => 'This domain is already active within our system and therefore cannot be processed.',
      263 => 'Invalid number of years, or no years provided.',
      264 => 'Domain cannot be renewed for specified number of years (details provided in response)',
      265 => 'Domain cannot be transferred at this time (details provided in response)',
      266 => 'No domain transfer exists for this user for this domain',
      267 => 'Invalid domain name, or we do not support the provided extension/TLD.',
      280 => 'DNS modification error',
      300 => 'Successful API operation',
      301 => 'Successful registration, but not all provided hosts were valid resulting in our nameservers being used',
      302 => 'Successful order, but there was an error with the contact information provided so your account default contact profile was used',
      400 => 'Existing API request is still processing - request will need to be re-submitted',
    );

    return empty($codes[intval($code)]) ?
      'Unhandled response code: ' . $code :
      $codes[intval($code)];
  }
}
