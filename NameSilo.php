<?php
/**
 * @file
 * Class source for NameSilo.com API functionality.
 */

namespace NameSilo;

/**
 * NameSilo.com API wrapper.
 *
 * Provides basic API handling for NameSilo.com.
 *
 * @author Steven Wichers
 * @version 1
 * @class
 */
class NameSilo extends NameSiloCommunicator {

  /**
   * List available domains on the given NameSilo account.
   *
   * @return mixed An array of domains on success or FALSE on failure.
   */
  public function listDomains() {

    $result = $this->fetch('listDomains');

    if (empty($result)) {

      return FALSE;
    }

    $domains = (array)$result->domains->domain;

    return $domains;
  }

  /**
   * List available DNS records for the given domain.
   *
   * @param string $domain The domain to retrieve DNS records for.
   *
   * @return mixed An array of NameSiloDNSRecord objects or FALSE on failure.
   *
   * @see NameSiloDNSRecord for further result documentation.
   */
  public function dnsListRecords($domain) {

    $result = $this->fetch('dnsListRecords', array(
      'domain' => $domain,
    ));

    if (empty($result)) {

      return FALSE;
    }

    $records = array();

    foreach ($result->resource_record as $record) {

      $records[(string)$record->record_id] = new NameSiloDNSRecord($this->_api_key, $domain, (array)$record);
    }

    return $records;
  }
}
