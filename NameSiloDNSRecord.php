<?php
/**
 * @file
 * Class source for handling DNS records from NameSilo.com
 */

namespace NameSilo;

/**
 * NameSilo.com DNS record wrapper.
 *
 * Supports DNS record validation and updating.
 *
 * @author Steven Wichers
 * @version 1
 * @class
 */
class NameSiloDNSRecord extends NameSiloCommunicator {

  /**
   * An array that contains the components of a DNS record.
   * @var array
   */
  protected $_resource_record = NULL;

  /**
   * The domain this DNS record belongs to.
   * @var string
   */
  protected $_domain = NULL;

  /**
   * A flag that holds whether or not this record has been modified since creation.
   * @var boolean
   */
  protected $_record_modified = FALSE;

  /**
   * Creates a NameSiloDNSRecord object.
   *
   * @param string $api_key The API key to use for communication with NameSilo.com.
   * @param string $domain The domain this DNS record belongs to.
   * @param array $resource_record The DNS record.
   */
  public function __construct($api_key, $domain, array $resource_record) {

    if (empty($domain) || FALSE === parse_url($domain)) {

      throw new Exception('Invalid domain supplied: ' . $domain);
    }

    parent::__construct($api_key);
    $this->_domain = $domain;
    $this->setRecord($resource_record);
  }

  /**
   * Validates DNS record arrays.
   *
   * Verifies that certain aspects of the array are sane, such as the DNS type,
   * ttl, and record ID values. It does not verify that the data is valid as
   * according to NameSilo.com
   *
   * @param array $resource_record The record data to validate.
   *
   * @return bool TRUE if valid, FALSE otherwise.
   */
  protected function validateRecord(array $resource_record) {

    $valid_record_types = array(
      'A', 'AAAA', 'AFSDB', 'ATMA', 'CNAME', 'HINFO', 'ISDN', 'KEY', 'MB', 'MG',
      'MINFO', 'MR', 'MX', 'NS', 'NXT', 'OPT', 'PTR', 'RP', 'RT', 'SIG', 'SOA',
      'SRV', 'TXT','WKS', 'X25'
    );

    if (empty($resource_record['record_id']) || !ctype_xdigit($resource_record['record_id'])) {

      throw new Exception('Invalid record_id: ' . $resource_record['record_id']);
    }
    elseif (empty($resource_record['type']) || !in_array($resource_record['type'], $valid_record_types)) {

      throw new Exception('Invalid record type: ' . $resource_record['type']);
    }
    elseif (!empty($resource_record['ttl']) && intval($resource_record['ttl']) != $resource_record['ttl']) {

      throw new Exception('Record TTL value was not an integer: ' . $resource_record['ttl']);
    }
    elseif (!isset($resource_record['host'])) {

      throw new Exception('Record host must be set.');
    }

    return TRUE;
  }

  /**
   * Getter method to retrieve the DNS record.
   *
   * @return array The resource record.
   */
  public function getRecord() {

    return $this->_resource_record;
  }

  /**
   * Setter method to modify the DNS record.
   *
   * @param array $new_record The new DNS record values.
   *
   * @return bool TRUE if the changes were accepted, FALSE otherwise.
   */
  public function setRecord(array $new_record) {

    // Only update the value if there are changes to begin with.
    if ($this->_resource_record == $new_record) {

      return FALSE;
    }
    elseif (!$this->validateRecord($new_record)) {

      /*
        Realistically should never be called given that the validate function
        throws exceptions for all problems.
      */
      throw new Exception('Invalid DNS record given.');
    }

    // This check is necessary when this method is called from the constructor.
    if (!empty($this->_resource_record)) {

      // Flip our modified flag for use in the save method.
      $this->_record_modified = TRUE;
    }

    $this->_resource_record = $new_record;

    return TRUE;
  }

  /**
   * Getter for the domain property.
   *
   * @return string
   */
  public function getDomain() {

    return $this->_domain;
  }

  /**
   * Check if the DNS record has been modified.
   *
   * @return boolean TRUE if modified, FALSE otherwise.
   */
  public function isModified() {

    return $this->_record_modified;
  }

  /**
   * Save the DNS record to NameSilo.com
   *
   * @return mixed A SimpleXML object or FALSE on failure.
   *
   * @see NameSiloDNSRecord::dnsUpdateRecord() for more information.
   */
  public function save() {

    // Exit early if there are no changes to save.
    if (!$this->isModified()) {

      return FALSE;
    }

    /**
     * The actual DNS record information that gets saved.
     * @var array
     */
    $modified_record = $this->_resource_record;

    /**
     * Flag to check if the DNS record host ends with the DNS record domain.
     * @var bool
     */
    $host_ends_with_domain = substr($modified_record['host'], -strlen($this->_domain)) == $this->_domain;

    /**
     * The host needs the domain stripped out to prevent doubling up of domains.
     *
     * The dnsUpdateRecord API call assumes the host does not contain the domain,
     * but the dnsListRecords call returns the host with the domain in it.
     */
    if ($host_ends_with_domain) {

      $modified_record['host'] = substr($modified_record['host'], 0, -strlen($this->_domain) - 1);
      $modified_record['host'] = trim($modified_record['host'], '.');
    }

    return $this->dnsUpdateRecord($this->_domain, $modified_record);
  }

  /**
   * Update a DNS record for the given domain.
   *
   * Warning:
   *
   * The dnsUpdateRecord API call assumes the host does not contain the domain,
   * but the dnsListRecords call returns the host with the domain in it.
   *
   * @param string $domain The domain to update the DNS record for.
   * @param array $resource_record The DNS record information to use.
   *
   * @return mixed A SimpleXML object or FALSE on failure.
   */
  protected function dnsUpdateRecord($domain, array $resource_record) {

    if (!$this->validateRecord($resource_record)) {

      return FALSE;
    }

    $result = $this->fetch('dnsUpdateRecord', array(
      'domain' => $domain,
      'rrid' => $resource_record['record_id'],
      'rrhost' => $resource_record['host'],
      'rrvalue' => $resource_record['value'],
      'rrttl' => $resource_record['ttl'],
      // Only used for MX records.
      'rrdistance' => $resource_record['distance'],
    ));

    return $result;
  }
}
